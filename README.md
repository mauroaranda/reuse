<!--
SPDX-FileCopyrightText: 2021-2023 Mauro Aranda <maurooaranda@gmail.com>
SPDX-License-Identifier: GPL-3.0-or-later
 -->
# reuse: Run REUSE from within Emacs

> Emacs interface to the reuse tool (https://reuse.software/)

## Table of Contents

- [Installation](#installation)
- [Using](#using)
- [License](#license)

---

## Installation
You can follow these steps:

- Download and add the directory to your load path:
`(add-to-list 'load-path "/the/reuse/dir")`

Done!

---

## Using

For a transient UI: `M-x reuse`

For a non-transient UI: `M-x reuse-COMMAND`

See the Info file for more information.

---

## License

- **[GPL 3](https://www.gnu.org/licenses/gpl-3.0-standalone.html)**
- Copyright 2021-2023 Mauro Aranda
