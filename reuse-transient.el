;;; reuse-transient.el --- Transient support for reuse -*- lexical-binding: t -*-

;; SPDX-FileCopyrightText: 2021-2023 Mauro Aranda <maurooaranda@gmail.com>
;; SPDX-License-Identifier: GPL-3.0-or-later

;; This file is NOT part of GNU Emacs.

;; reuse is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 3
;; of the License, or (at your option) any later version.
;;
;; reuse is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with reuse. If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Transient support for reuse.

;;; Code:
;; Requirements.
(require 'eieio)
(require 'cl-lib)
(require 'reuse)
(require 'transient)

(declare-function reuse-addheader "reuse")
(declare-function reuse-download "reuse")
(declare-function reuse-init "reuse")
(declare-function reuse-lint "reuse")
(declare-function reuse-spdx "reuse")
(declare-function reuse-version "reuse")

;; Variables.
(defvar reuse-global-args nil
  "An alist that holds global arguments for the next execution of reuse.
The keys are the names of the arguments, and the values are the argument
values.

This variable is only meaningful when using transient.")

(defvar reuse-global-switches nil
  "A list that holds global switches for the next execution of reuse.
Each element is a switch name.

This variable is only meaningul when using transient.")

;; Utils.
(defun reuse-globals ()
  "Return the global arguments for running reuse."
  (append (flatten-tree reuse-global-args) reuse-global-switches))

;; Transient support.
(defclass reuse-transient-global-argument (reuse-command-argument
                                           transient-variable)
  ())

(defclass reuse-transient-global-switch (reuse-command-switch
                                         transient-switch)
  ())

(cl-defmethod transient-infix-set ((obj reuse-transient-global-argument)
                                   value)
  "Set the key named after the name of OBJ to VALUE in `reuse-global-args'."
  (setf (alist-get (oref obj name) reuse-global-args) value))

(cl-defmethod transient-infix-set ((obj reuse-transient-global-switch) _value)
  "Add to or remove from `reuse-global-switches' the argument slot of OBJ."
  (if (member (oref obj argument) reuse-global-switches)
      (setq reuse-global-switches (delete (oref obj argument)
                                          reuse-global-switches))
    (push (oref obj argument) reuse-global-switches)))

(cl-defmethod transient-format-value ((obj reuse-transient-global-argument))
  "Format OBJ as a command-line argument with an option, when active."
  (let ((value (alist-get (oref obj name) reuse-global-args nil)))
    (propertize (format (if value "%s=%s" "%s")
                        (oref obj name)
                        value)
                'face
                (if value 'transient-value 'transient-inactive-value))))

(cl-defmethod transient-format-value ((obj reuse-transient-global-switch))
  "Format OBJ as a command-line argument with an option, when active."
  (propertize (oref obj argument)
              'face
              (if (member (oref obj argument) reuse-global-switches)
                  'transient-value
                'transient-inactive-value)))

(transient-define-argument reuse-root ()
  :description "Set project root"
  :class 'reuse-transient-global-argument
  :name "--root"
  :argument ""
  :reader 'transient-read-directory
  :key "-r")

(transient-define-infix reuse-multiprocessing ()
  :description "Do not use multiprocessing"
  :class 'reuse-transient-global-switch
  :argument "--no-multiprocessing"
  :key "-m")

(transient-define-infix reuse-include-submodules ()
  :description "Do not skip over Git submodules"
  :class 'reuse-transient-global-switch
  :argument "--include-submodules"
  :key "-s")

(transient-define-infix reuse-debug ()
  :description "Enable debug statements"
  :class 'reuse-transient-global-switch
  :argument "--debug"
  :key "-d")

(transient-define-argument reuse-dir ()
  :description "Directory for init"
  :class 'transient-option
  :argument ""
  :reader 'transient-read-directory
  :key "-d")

(transient-define-argument reuse-license-output ()
  :description "Select output file for license" 
  :class 'transient-option
  :argument "--output="
  :key "-o")

;;;###autoload
(transient-define-prefix reuse ()
  "Invoke a reuse command from the list of available commands.
You can also specify some global arguments for the next execution of reuse."
  ["Arguments"
   (reuse-root)
   (reuse-multiprocessing)
   (reuse-include-submodules)
   (reuse-debug)]
  ["Run"
   (5 "v" "Show reuse version and exit" reuse-do-version)
   ("a" "Add header (copyright and/or license) to a list of files"
    reuse-transient-addheader)
   ("d" "Download a license used in the project" reuse-transient-download)
   ("i" "Initialize REUSE in the project" reuse-transient-init)
   ("l" "Lint the project for REUSE compliance" reuse-do-lint)
   ("s" "Print the project's bill of materials, in SPDX format"
    reuse-transient-spdx)]
  (interactive)
  ;; Reset.
  (setq reuse-global-args nil)
  (setq reuse-global-switches nil)
  (unwind-protect
      (transient-setup 'reuse)
    ;; FIXME: Is this necessary?
    (setq reuse-global-args nil)
    (setq reuse-global-switches nil)))

(transient-define-prefix reuse-transient-addheader ()
  "Pass arguments to the addheader command, and run it."
  ["Arguments"
   ("-y" "Select year for copyright statement" "--year=")
   ("-Y" "Do not include year in statement" "--exclude-year")
   ("-S" "Select the comment style to use" "--style")
   ("-s" "Force single-line comment style" "--single-line")
   ("-m" "Force multi-line comment style" "--multi-line")
   ("-e" "Place header in a .license file" "--explicit-license")
   ("-k" "Skip files with unrecognised comment styles" "--skip-unrecognised")
   ("-C" "Select the copyright style to use", "--copyright-style=")
   ("-t" "Select the template to use" "--template=")]
  ["Run"
   ("a" "Add license header to a file" reuse-do-addheader)])

(transient-define-suffix reuse-do-addheader (args)
  "Add license header to files, using ARGS from `reuse-transient-addheader'."
  (interactive (list (transient-args 'reuse-transient-addheader)))
  (let ((required (flatten-tree
                   (append
                    (mapcar (lambda (license)
                              (format "--license=%s" license))
                            (reuse--read-appendable-argument
                             (apply-partially #'read-string
                                              "License name: ")
                             #'string-empty-p))
                    (mapcar (lambda (copyright)
                              (format "--copyright=%s" copyright))
                            (reuse--read-appendable-argument
                             (apply-partially
                              #'read-string "Copyright statement: ")
                             #'string-empty-p))
                    (mapcar (lambda (filename)
                              (file-expand-wildcards
                               (expand-file-name filename)))
                            (reuse--read-appendable-argument
                             (apply-partially #'read-file-name
                                              "File: ")
                             #'string-empty-p))))))
    (reuse-addheader (reuse-globals) (append args required))))

(transient-define-prefix reuse-transient-download ()
  "Pass arguments to the download command, and run it."
  ["Arguments"
   ("-a" "Download all missing licenses detected" "--all")
   (reuse-license-output)]
  ["Run"
   ("c" "Download license as COPYING" reuse-do-download-license-as-copying)
   ("d" "Download license file" reuse-do-download)])

(transient-define-suffix reuse-do-download (args)
  "Download licenses, using `reuse-transient-download' ARGS."
  (interactive (list (transient-args 'reuse-transient-download)))
  (unless (member "--all" args)
    (setq args (append args (reuse--read-appendable-argument
                             (apply-partially #'read-string "License: ")
                             #'string-empty-p))))
  (reuse-download (reuse-globals) args))

(transient-define-suffix reuse-do-download-license-as-copying (license)
  "Download a license read from the minibuffer and save it as COPYING."
  (interactive (list (read-string "License: ")))
  (reuse-download-license-as-copying license))

(transient-define-prefix reuse-transient-init ()
  "Pass arguments to the init command, and run it."
  ["Arguments"
   (reuse-dir)]
  ["Run"
   ("i" "Initialize REUSE in the project" reuse-do-init)]
  (interactive)
  (transient-setup 'reuse-transient-init))

(transient-define-suffix reuse-do-init (args)
  "Initialize REUSE in the project, using `reuse-transient-init' ARGS."
  (interactive (list (transient-args 'reuse-transient-init)))
  (reuse-init (reuse-globals) args))

(transient-define-suffix reuse-do-lint ()
  "Lint the project for REUSE compliance."
  (interactive)
  (reuse-lint (reuse-globals) nil))

(transient-define-prefix reuse-transient-spdx ()
  "Pass arguments to the spdx command, and run it."
  ["Arguments"
   (reuse-license-output)]
  ["Run"
   ("s" "Run reuse spdx" reuse-do-spdx)])

(transient-define-suffix reuse-do-spdx (args)
  "Print a SPDX bill of materials, using `reuse-transient-spdx' ARGS"
  (interactive (list (transient-args 'reuse-transient-spdx)))
  (reuse-spdx (reuse-globals) args))

(transient-define-suffix reuse-do-version ()
  "Print REUSE version in the echo area."
  (interactive)
  (reuse-version))

(provide 'reuse-transient)
;;; reuse-transient.el ends here
