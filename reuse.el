;;; reuse.el --- REUSE interface for Emacs -*- lexical-binding: t -*-

;; Author: Mauro Aranda <maurooaranda@gmail.com>
;; Maintainer: Mauro Aranda <maurooaranda@gmail.com>
;; Created: Tue Feb 24 07:42:00 2021
;; URL: https://gitlab.com/maurooaranda/reuse
;; Version: 0.1
;; Package-Version: 0.1
;; Package-Requires: ((emacs "27.1"))
;; Keywords: tools

;; SPDX-FileCopyrightText: 2021-2023 Mauro Aranda <maurooaranda@gmail.com>
;; SPDX-License-Identifier: GPL-3.0-or-later

;; This file is NOT part of GNU Emacs.

;; reuse is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 3
;; of the License, or (at your option) any later version.
;;
;; reuse is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with reuse. If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Run reuse-tool commands from within Emacs.

;; See more information about the REUSE project and the reuse-tool program
;; here: https://reuse.software

;;;; Usage
;; You can run all the reuse subcommands by typing:
;; M-x reuse-COMMAND
;; For example, to run the reuse linter, type: M-x reuse-lint
;;
;; Alternatively, if you have `transient' installed
;; and you like what it offers, then run the following: M-x reuse
;; After that, select the command you want to run.
;;
;; The actual function doing the job is the same, so the only thing that
;; changes between the two ways of running the reuse commands is the
;; interface (and that you have to require reuse-transient in your init file).
;; With transient, you specify the global arguments and the specific arguments
;; to the reuse command with a combination of transient infixes and the
;; minibuffer.  Without transient, you have to use `universal-argument' to get
;; prompted about non-default optional arguments.  With C-u M-x reuse-COMMAND,
;; you can pass the subcommand arguments; while with C-u C-u M-x reuse-COMMAND
;; you can pass also the global arguments.
;;
;; As you may have guessed, the later may make running the reuse commands very
;; tedious: that's why this package provide commands to run reuse in common
;; use cases.  For example, you can: M-x reuse-download-license-as-copying
;; to download a license and put it in a COPYING file.
;;
;; Also, please note that while reuse provides addheader for adding the spdx
;; headers, you might prefer to use the Emacs editing capabilities to do that.

;;; TODO:
;; - Completion support.

;;; Code:

;; Requirements.
(require 'eieio)
(require 'cl-lib)
(require 'subr-x) ; string-empty-p.

(declare-function project-roots "project.el")
(declare-function widget-create-child-and-convert "wid-edit.el")

;; Customization.
(defgroup reuse nil
  "Run reuse from within Emacs."
  :group 'tools
  :version "0.1")

(defcustom reuse-command-name (executable-find "reuse")
  "Location of the reuse command to use."
  :type '(choice (const :tag "Not installed" nil)
                 (string :tag "Executable"))
  :package-version '(reuse . "0.1"))

(defcustom reuse-default-reuse-arguments nil
  "Default arguments to pass to the reuse program.

A list of strings, each one being a valid argument for the reuse program.
These arguments can be overriden with a prefix of two universal arguments."
  :type '(repeat string)
  :package-version '(reuse . "0.1"))

(defcustom reuse-default-subcommand-arguments nil
  "Default arguments to pass to a reuse subcommand.

The value is an alist, where the keys are reuse subcommand names
and the values are a list of strings, each string being a valid argument for
the subcommand.
The arguments can be overriden with a prefix of one universal argument."
  :type '(alist :key-type string
                :value-type (repeat string))
  :package-version '(reuse . "0.1"))

(defcustom reuse-init-interface 'interactive
  "What interface to use when running reuse init.
The current supported values are:
- interactive, which means you interact with reuse init as if you had executed
it from the command line.
- widget, whiche means to display a buffer with widgets, in a GUI-like style."
  :type '(choice (const :tag "Interactive prompt" interactive)
                 (const :tag "Widget UI" widget))
  :package-version '(reuse . "0.1"))

;; Variables.
(defclass reuse-command-option ()
  ((name :initarg :name)
   (reuse-reader :initarg :reuse-reader)
   (reuse-desc :initarg :reuse-desc :type string)
   (choice-key :initarg :choice-key :type character)
   (filter :initarg :filter :type (or function null) :initform nil)))

;; Global args save their values into an alist.
(defclass reuse-command-argument (reuse-command-option)
  ())

;; Global switches push/pop their argument names into a list.  The presence
;; in the list means the switch is on.
(defclass reuse-command-switch (reuse-command-option)
  ())

;; A class to represent a null choice,
;; for use with choices which all are optional.
(defclass reuse-command-switch-null (reuse-command-option)
  ())

;; When two or more arguments are mutually exclusive, use this class.
(defclass reuse-command-choice-option (reuse-command-option)
  ((choices :initarg :choices :type list)
   (read-choices :initarg :read-choices :type list)))

(defclass reuse-command-required-option (reuse-command-option)
  ())

(defclass reuse-command ()
  ((name :initarg :name :type string)
   (options :initarg :options :initform nil :type list)))

(cl-defmethod reuse-format-option-value ((self reuse-command-argument) value)
  "Format SELF and VALUE as a command line argument: \"--argument=value\"."
  (format "--%s=%s" (oref self name) value))

(cl-defmethod reuse-format-option-value ((self reuse-command-switch) value)
  "When VALUE is non-nil, format SELF as a command line switch: \"--switch\"."
  (when value
    (format "--%s" (oref self name))))

(cl-defmethod reuse-format-option-value ((_self reuse-command-switch-null)
                                         _value)
  "Ignore SELF and VALUE and return nil."
  nil)

(cl-defgeneric reuse-read-reuse-option (self &rest values)
  "Read a reuse option SELF, using VALUES to filter the option.

To filter the option means whether to actually read the option or not.")

(cl-defmethod reuse-read-reuse-option :around ((self reuse-command-option)
                                               &rest values)
  "Filter SELF against VALUES, and call the primary function if not filtered."
  (let ((filter (oref self filter)))
    (unless (and filter values (apply filter values))
      (cl-call-next-method self values))))

(cl-defmethod reuse-read-reuse-option ((self reuse-command-argument)
                                       &rest _values)
  "Ignore VALUES and read the value for SELF."
  (when-let ((value (funcall (oref self reuse-reader))))
    (reuse-format-option-value self value)))

(cl-defmethod reuse-read-reuse-option ((self reuse-command-switch)
                                       &rest _values)
  "Ignore VALUES and read the value for SELF."
  (reuse-format-option-value self (funcall (oref self reuse-reader))))

(cl-defmethod reuse-read-reuse-option ((_self reuse-command-switch-null)
                                       &rest _values)
  "Ignore SELF and VALUES, since a null switch option doesn't have any value."
  nil)

(cl-defmethod reuse-read-reuse-option ((self reuse-command-choice-option)
                                       &rest _values)
  "Ignore VALUES and read the value for SELF."
  (let (choices)
    (dolist (choice (oref self choices))
      (push (list (oref choice choice-key) (oref choice name)
                  (oref choice reuse-desc))
            choices))
    (when-let ((value (cadr (funcall (oref self reuse-reader) choices))))
      (reuse-read-reuse-option (cl-find-if (lambda (el)
                                             (string= (oref el name) value))
                                           (oref self choices))))))

(cl-defmethod reuse-read-reuse-option ((self reuse-command-required-option)
                                       &rest _values)
  "Ignore VALUES and read the value for SELF."
  (let ((value (funcall (oref self reuse-reader))))
    (or value (user-error "Argument %s is required" (oref self name)))))

(defvar reuse-commands
  (list
   (make-instance 'reuse-command
                  :name "reuse"
                  :options
                  (list (make-instance 'reuse-command-argument
                                       :name "root"
                                       :reuse-reader
                                       (apply-partially
                                        #'reuse--read-directory-name
                                        "Project root: "))
                        (make-instance 'reuse-command-switch
                                       :name "debug"
                                       :reuse-reader
                                       (apply-partially
                                        #'y-or-n-p "Enable debug statements? "))
                        (make-instance 'reuse-command-switch
                                       :name "no-multiprocessing"
                                       :reuse-reader
                                       (apply-partially
                                        #'y-or-n-p "Use multiprocessing? "))
                        (make-instance 'reuse-command-switch
                                       :name "include-submodules"
                                       :reuse-reader
                                       (apply-partially
                                        #'y-or-n-p
                                        "Include Git submodules? "))))
   (make-instance 'reuse-command
                  :name "addheader"
                  :options
                  (list
                   (make-instance 'reuse-command-choice-option
                                  :name "year"
                                  :reuse-reader
                                  (apply-partially
                                   #'read-multiple-choice "Year option: ")
                                  :choices
                                  (list
                                   (make-instance 'reuse-command-switch
                                                  :name "exclude-year"
                                                  :reuse-reader
                                                  (lambda (&rest ignored)
                                                    t)
                                                  :reuse-desc "Exclude year"
                                                  :choice-key ?e)
                                   (make-instance 'reuse-command-argument
                                                  :name "year"
                                                  :reuse-reader
                                                  (apply-partially
                                                   ;; It can be a range,
                                                   ;; so no `read-number'.
                                                   #'reuse--read-string
                                                   "Year: ")
                                                  :reuse-desc "Input a year"
                                                  :choice-key ?y)
                                   (make-instance 'reuse-command-switch-null
                                                  :name "don't-set"
                                                  :reuse-reader #'ignore
                                                  :reuse-desc
                                                  "Do not set this option"
                                                  :choice-key ?n)))
                   (make-instance 'reuse-command-choice-option
                                  :name "comment"
                                  :reuse-reader
                                  (apply-partially
                                   #'read-multiple-choice
                                   "Single or multi-line comment? ")
                                  :choices
                                  (list
                                   (make-instance 'reuse-command-switch
                                                  :name "single-line"
                                                  :reuse-reader
                                                  (lambda (&rest ignored)
                                                    t)
                                                  :reuse-desc
                                                  "Force single line comment"
                                                  :choice-key ?s)
                                   (make-instance 'reuse-command-switch
                                                  :name "multi-line"
                                                  :reuse-reader
                                                  (lambda (&rest ignored)
                                                    t)
                                                  :reuse-desc
                                                  "Force multi line comment"
                                                  :choice-key ?m)
                                   (make-instance 'reuse-command-switch-null
                                                  :name "no-force"
                                                  :reuse-reader #'ignore
                                                  :reuse-desc
                                                  "Do not force"
                                                  :choice-key ?n)))
                   (make-instance 'reuse-command-switch
                                  :name "explicit-license"
                                  :reuse-reader
                                  (apply-partially
                                   #'y-or-n-p
                                   "Place header in a .license file? "))
                   (make-instance
                    'reuse-command-switch
                    :name "skip-unrecognised"
                    :reuse-reader
                    (apply-partially
                     #'y-or-n-p
                     "Skip files with unrecognised comment styles? "))
                   (make-instance 'reuse-command-argument
                                  :name "style"
                                  :reuse-reader
                                  (apply-partially
                                   #'reuse--read-string "Comment style: "))
                   (make-instance 'reuse-command-argument
                                  :name "copyright-style"
                                  :reuse-reader
                                  (apply-partially
                                   #'reuse--read-string
                                   "Copyright style: "))
                   (make-instance 'reuse-command-argument
                                  :name "template"
                                  :reuse-reader
                                  (apply-partially
                                   #'reuse--read-file-name
                                   "Template file: "))))
   (make-instance 'reuse-command
                  :name "download"
                  :options
                  (list
                   (make-instance 'reuse-command-choice-option
                                  :name "licenses"
                                  :reuse-reader
                                  (apply-partially #'read-multiple-choice
                                                   "Download all? ")
                                  :choices
                                  (list
                                   (make-instance
                                    'reuse-command-switch
                                    :name "all"
                                    :reuse-reader
                                    (lambda (&rest ignored)
                                      t)
                                    :reuse-desc
                                    "Download all missing licenses"
                                    :choice-key ?a)
                                   (make-instance
                                    'reuse-command-required-option
                                    :name "licenses"
                                    :reuse-reader
                                    (apply-partially
                                     #'reuse--read-appendable-argument
                                     (apply-partially #'read-string
                                                      "Licenses: ")
                                     #'string-empty-p)
                                    :reuse-desc
                                    "Download the given licenses"
                                    :choice-key ?s)))
                   (make-instance 'reuse-command-choice-option
                                  :name "output?"
                                  :reuse-reader
                                  (apply-partially #'read-multiple-choice
                                                   "Output file? ")
                                  :filter
                                  (lambda (&rest values)
                                    (when-let ((licenses
                                                (alist-get "licenses" values
                                                           nil nil
                                                           #'string=)))
                                      (or (and (stringp licenses)
                                               (string= licenses "--all"))
                                          (nthcdr 1 licenses))))
                                  :choices
                                  (list
                                   (make-instance 'reuse-command-argument
                                                  :name "output"
                                                  :reuse-reader
                                                  (apply-partially
                                                   #'reuse--read-file-name
                                                   "Output file: " nil nil nil
                                                   "COPYING")
                                                  :reuse-desc
                                                  "Specify an output file"
                                                  :choice-key ?o)
                                   (make-instance 'reuse-command-switch-null
                                                  :name "no"
                                                  :reuse-reader #'ignore
                                                  :reuse-desc "No output file"
                                                  :choice-key ?n)))))
   (make-instance 'reuse-command
                  :name "init"
                  :options
                  (list (make-instance 'reuse-command-argument
                                       :name "path"
                                       :reuse-reader
                                       (apply-partially
                                        #'reuse--read-directory-name
                                        "Directory: "))))
   (make-instance 'reuse-command :name "lint")
   (make-instance 'reuse-command
                  :name "spdx"
                  :options
                  (list (make-instance 'reuse-command-argument
                                       :name "output"
                                       :reuse-reader
                                       (apply-partially
                                        #'reuse--read-file-name
                                        "Output file: " nil
                                        nil nil "report.spdx")))))
  "All the reuse commands, as instances of the `reuse-command' class.")

;; Utils.
(defun reuse--read-directory-name (prompt)
  "Prompt with PROMPT to read a directory, as needed by reuse options."
  (let ((dir (read-directory-name prompt)))
    (if (string-empty-p dir)
        (expand-file-name default-directory)
      (expand-file-name (file-name-as-directory dir)))))

(defun reuse--read-file-name (prompt &optional dir default-filename mustmatch
                                     initial predicate)
  "Prompt with PROMPT to read a filename, as needed by reuse options.

Pass DIR, DEFAULT-FILENAME, MUSTMATCH, INITIAL and PREDICATE to
`read-file-name'."
  (let ((filename (read-file-name prompt dir default-filename mustmatch
                                  initial predicate)))
    (unless (string-empty-p filename)
      (expand-file-name filename))))

(defun reuse--read-string (prompt)
  "Prompt with PROMPT to read a string, as needed by reuse options."
  (let ((str (read-string prompt)))
    (unless (string-empty-p str)
      str)))

(defun reuse--read-appendable-argument (read-fn done-fn)
  "Read arguments using READ-FN, and return the read arguments in a list.
DONE-FN should return non-nil for the value that means to stop reading
arguments."
  (catch :done
    (let (val acc)
      (while t
        (if (funcall done-fn (setq val (funcall read-fn)))
            (throw :done (reverse acc))
          (push val acc))))))

(defun reuse--init-sentinel (proc _msg)
  "Sentinel for the reuse process PROC.

If the buffer gets killed, sets the process buffer to nil.
Else if the process received a signal or exited, get the exit code and
print a message to the user."
  (cond ((null (buffer-name (process-buffer proc)))
         (set-process-buffer proc nil))
        ((memq (process-status proc) '(exit signal))
         (kill-buffer (process-buffer proc))
         (let ((exit-code (process-exit-status proc)))
           (cond ((= exit-code 0)
                  (message "reuse init finished succesfully"))
                 (t (message "reuse init finished abnormally")))))))

;; Functions.
(defun reuse-read-command-args (command)
  "Prompt the user for each option the reuse command COMMAND supports."
  (when-let ((obj (cl-find-if (lambda (el)
                                (string= command (oref el name)))
                              reuse-commands)))
    (let (result input acc)
      (dolist (option (oref obj options))
        (when (setq input (apply #'reuse-read-reuse-option option acc))
          (push (cons (oref option name) input) acc)
          (push input result)))
      (flatten-tree result))))

(defun reuse-run-reuse (&rest args)
  "Run reuse synchronously, with arguments ARGS.
Messages the command output, or a default message when the command produces no
output."
  (unless reuse-command-name
    (error "Couldn't find the reuse executable"))
  (let ((reuse-buff "*Reuse*"))
    (apply #'call-process reuse-command-name nil reuse-buff nil args)
    (with-current-buffer reuse-buff
      (let ((output (string-trim (buffer-string))))
        (if (string-empty-p output)
            (message "Command exited with no output")
          (message "%s" (string-trim (buffer-string))))))
    (kill-buffer reuse-buff)))

;;;; Basic commands.
;;;###autoload
(defun reuse-addheader (global-args args)
  "Run reuse addheader with arguments GLOBAL-ARGS and ARGS.
With a prefix argument, prompt for the relevant arguments for the addheader
command."
  (interactive (let* ((globals (if (and current-prefix-arg
                                        (car-safe current-prefix-arg)
                                        (= (car current-prefix-arg) 16))
                                   (reuse-read-command-args "reuse")
                                 reuse-default-reuse-arguments))
                      (locals (if current-prefix-arg
                                  (reuse-read-command-args "addheader")
                                (alist-get "addheader"
                                           reuse-default-subcommand-arguments
                                           nil nil #'string=)))
                      (required (flatten-tree
                                 (append
                                  (mapcar (lambda (license)
                                            (format "--license=%s" license))
                                          (reuse--read-appendable-argument
                                           (apply-partially #'read-string
                                                            "License name: ")
                                           #'string-empty-p))
                                  (mapcar
                                   (lambda (copyright)
                                     (format "--copyright=%s" copyright))
                                   (reuse--read-appendable-argument
                                    (apply-partially
                                     #'read-string "Copyright statement: ")
                                    #'string-empty-p))
                                  (mapcar (lambda (filename)
                                            (file-expand-wildcards
                                             (expand-file-name filename)))
                                          (reuse--read-appendable-argument
                                           (apply-partially #'read-file-name
                                                            "File: ")
                                           #'string-empty-p))))))
                 (list globals (append locals required))))
  (apply #'reuse-run-reuse (append global-args (list "addheader") args)))

;;;###autoload
(defun reuse-download (global-args args)
  "Run reuse download with arguments GLOBAL-ARGS and ARGS.
With a prefix argument, prompt for the relevant arguments for the
download command."
  (interactive (let* ((globals (if (and current-prefix-arg
                                        (car-safe current-prefix-arg)
                                        (= (car current-prefix-arg) 16))
                                   (reuse-read-command-args "reuse")
                                 reuse-default-reuse-arguments))
                      (locals (if current-prefix-arg
                                  (reuse-read-command-args "download")
                                (alist-get "download"
                                           reuse-default-subcommand-arguments
                                           nil nil #'string=)))
                      (required (unless locals
                                  (reuse--read-appendable-argument
                                   (apply-partially #'read-string "License: ")
                                   #'string-empty-p))))
                 (list globals (append locals required))))
  (apply #'reuse-run-reuse (append global-args (list "download") args)))

(defun reuse-init-widget-ui (global-args args)
  "Show a buffer for the user to run/interact with \"reuse init\".

Runs \"reuse init\" asynchronously, with GLOBAL-ARGS and ARGS send to
`reuse-command-name'.  When the user hits the button to send the data, sends it
and deactivates the button."
  (unless reuse-command-name
    (error "Couldn't find the reuse executable"))
  (require 'wid-edit)
  ;; Do some cleaning first.
  (switch-to-buffer "*Reuse Init*")
  (kill-all-local-variables)
  (let ((inhibit-read-only t))
    (erase-buffer))
  (remove-overlays)
  (condition-case nil
      (with-current-buffer "*Reuse init process*"
        (erase-buffer))
    (error nil))
  (let ((widget (widget-create 'group :format "%v"))
        (process (apply #'start-process "reuse-init" "*Reuse init process*"
                        reuse-command-name
                        (append global-args (list "init") args)))
        children)
    (set-process-sentinel process #'reuse--init-sentinel)
    (set-process-query-on-exit-flag process nil)
    (widget-put widget :reuse-process process)
    (widget-insert "*** Emacs Widget interface to REUSE ***\n\n")
    (push (widget-create-child-and-convert widget 'repeat
                                           :tag "Project licenses"
                                           '(string :tag "License"))
          children)
    (widget-insert "\n")
    (push (widget-create-child-and-convert widget 'string
                                           :tag "Project name")
          children)
    (widget-insert "\n")
    (push (widget-create-child-and-convert widget 'string
                                           :tag "Project URL")
          children)
    (widget-insert "\n")
    (push (widget-create-child-and-convert widget 'string
                                           :tag "Maintainer")
          children)
    (widget-insert "\n")
    (push (widget-create-child-and-convert widget 'string
                                           :tag "Maintainer E-mail")
          children)
    (widget-insert "\n\n")
    (widget-put widget :children (reverse children))
    (widget-create 'push-button
                   :action (lambda (w &rest _ignore)
                             (let ((values (widget-value widget)))
                               (message "Sending licenses...")
                               (dolist (license (car values))
                                 (accept-process-output process 1 nil t)
                                 (unless (string= license "")
                                   (process-send-string
                                    process (concat license "\n"))))
                               (message "Sending licenses...DONE")
                               (setq values (cdr values))
                               ;; reuse init expects an empty string to
                               ;; signal when there are no more licenses.
                               (process-send-string process "\n")
                               (message "Sending project information...")
                               (dolist (value values)
                                 (accept-process-output process 1 nil t)
                                 (process-send-string process
                                                      (concat value "\n")))
                               (message "Sending project information...DONE"))
                             (goto-char (point-max))
                             (widget-insert "\n\nProject initialized.")
                             (widget-apply w :deactivate))
                   "Send to command")
    (widget-insert "\t")
    (widget-create 'push-button
                   :action (lambda (&rest _ignore)
                             (kill-buffer (current-buffer)))
                   "Quit")
    (add-hook 'kill-buffer-hook (lambda ()
                                  (condition-case nil
                                      (delete-process process)
                                    (error nil)))
              nil t))
  (goto-char (point-min))
  (use-local-map widget-keymap)
  (widget-setup))

;;;###autoload
(defun reuse-init (global-args args)
  "Run reuse init, with arguments GLOBAL-ARGS and ARGS.
Selects the desired interaction with the init command according to the value
of the `reuse-init-interface' variable."
  (interactive (if current-prefix-arg
                   (list (when (and (car-safe current-prefix-arg)
                                    (= (car current-prefix-arg) 16))
                           (reuse-read-command-args "reuse"))
                         (reuse-read-command-args "init"))
                 (list reuse-default-reuse-arguments
                       (alist-get "init" reuse-default-subcommand-arguments
                                  nil nil #'string=))))
  (unless reuse-command-name
    (error "Couldn't find the reuse executable"))
  (cond ((eq reuse-init-interface 'interactive)
         (with-current-buffer (apply #'make-comint "reuse"
                                     reuse-command-name nil
                                     (append global-args (list "init") args))
           (let ((proc (get-process "reuse")))
             (set-process-sentinel proc #'reuse--init-sentinel)
             (set-process-query-on-exit-flag proc nil)
             ;; Wait until there is some output, to show a buffer with
             ;; some content to the user.
             (while (zerop (buffer-size))
               (sleep-for 1))
             (switch-to-buffer (current-buffer))
             proc)))
        ((eq reuse-init-interface 'widget)
         (reuse-init-widget-ui global-args args))))

;;;###autoload
(defun reuse-lint (global-args _args)
  "Run reuse lint with arguments GLOBAL-ARGS.
With a prefix argument, prompt for GLOBAL-ARGS.  The reuse lint command
currently takes no arguments."
  (interactive (if current-prefix-arg
                   (list
                    (when (and (car-safe current-prefix-arg)
                               (= (car current-prefix-arg) 16))
                      (reuse-read-command-args "reuse"))
                    (reuse-read-command-args "lint"))
                 (list reuse-default-reuse-arguments
                       (alist-get "lint" reuse-default-subcommand-arguments
                                  nil nil #'string=))))
  (apply #'reuse-run-reuse (append global-args (list "lint"))))

;;;###autoload
(defun reuse-spdx (global-args args)
  "Run reuse spdx with arguments GLOBAL-ARGS and ARGS.
With a prefix argument, promt for the relevant arguments for the spdx command."
  (interactive (if current-prefix-arg
                   (list
                    (when (and (car-safe current-prefix-arg)
                               (= (car current-prefix-arg) 16))
                      (reuse-read-command-args "reuse"))
                    (reuse-read-command-args "spdx"))
                 (list reuse-default-reuse-arguments
                       (alist-get "spdx" reuse-default-subcommand-arguments
                                  nil nil #'string=))))
  (apply #'reuse-run-reuse (append global-args (list "spdx") args)))

;;;; Handy commands.
(defun reuse-download-all ()
  "Run reuse download --all for the buffer file directory."
  (interactive)
  (reuse-download nil (list "--all")))

(defun reuse-download-license-as-copying (license)
  "Download LICENSE and save it into a file named COPYING, using reuse download.

The directory where the file COPYING is created is determined by the project
root, using the project library."
  (interactive (list (read-string "License: ")))
  (unless license
    (error "Must provide a license"))
  (require 'project)
  (let ((dir (car (project-roots (project-current)))))
    (reuse-download nil
                    (list (format "--output=%s" (expand-file-name
                                                 "COPYING"
                                                 (file-name-as-directory dir)))
                          license))))

(defun reuse-init-project (dir)
  "Run reuse init in directory DIR.  Read the directory from the minibuffer."
  (interactive "DProject to init: ")
  (reuse-init nil (list (expand-file-name (file-name-as-directory dir)))))

(defun reuse-lint-project (dir)
  "Run reuse lint in directory DIR.  Read the directory from the minibuffer."
  (interactive "DProject to lint: ")
  (reuse-lint (list (format "--root=%s" (expand-file-name
                                         (file-name-as-directory dir))))
              nil))

(defun reuse-spdx-project (dir)
  "Run reuse spdx in directory DIR.  Read the directory from the minibuffer."
  (interactive "DProject to lint: ")
  (reuse-spdx (list (format "--root=%s" (expand-file-name
                                         (file-name-as-directory dir))))
              nil))

(defun reuse-version ()
  "Print the reuse version in the minibuffer."
  (interactive)
  (reuse-run-reuse "--version"))

(provide 'reuse)
;;; reuse.el ends here
