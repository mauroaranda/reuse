# SPDX-FileCopyrightText: 2021-2023 Mauro Aranda <maurooaranda@gmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

# This file is part of reuse.

# reuse is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# reuse is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with reuse.  If not, see <https://www.gnu.org/licenses/>.

## Programs used.
EMACS = emacs
EMACSFLAGS = -batch -L "./" -f batch-byte-compile
MAKEINFO ?= makeinfo

## Variables (some might not be used right now).
PACKAGE = reuse
PACKAGE_BUGREPORT = maurooaranda@gmail.com
PACKAGE_NAME = reuse
PACKAGE_STRING = reuse 0.1
PACKAGE_TARNAME = reuse-0.1
PACKAGE_URL = 
PACKAGE_VERSION = 0.1
DISTDIR = $(PACKAGE_TARNAME)
DISTFILES = README.md Makefile reuse.el reuse-transient.el LICENSES/GPL-3.0-or-later.txt doclicense.texi docstyle.texi reuse.texi reuse.info

## Targets.

.PHONY: all info clean dist

all: reuse.elc reuse-transient.elc

reuse.elc: reuse.el
	$(EMACS) $(EMACSFLAGS) reuse.el

reuse-transient.elc: reuse-transient.el
	$(EMACS) $(EMACSFLAGS) reuse-transient.el

info: reuse.texi
	$(MAKEINFO) reuse.texi

clean:
	-rm -f reuse.elc reuse-transient.elc
	-rm -f reuse.info
	-rm -f $(PACKAGE_TARNAME).tar.gz

dist: reuse.elc
	mkdir --parents $(DISTDIR)
	cp --parents $(DISTFILES) $(DISTDIR)
	tar -cf $(PACKAGE_TARNAME).tar $(DISTDIR)
	rm -R $(DISTDIR)
	gzip $(PACKAGE_TARNAME).tar
